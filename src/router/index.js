import Vue from "vue";
import VueRouter from "vue-router";
import Main from "@/views/Main";

Vue.use(VueRouter);

const routes = [
  {
    path: "/screen",
    component: () => import("@/views/Screen"),
  },
  {
    path: "/",
    component: Main,
  },
  {
    path: "*",
    redirect: "/",
  },
];

const router = new VueRouter({
  routes,
  mode: "history",
});

export default router;
