import Vue from "vue";

import "./styles/quasar.scss";
import "quasar/dist/quasar.ie.polyfills";
import "@quasar/extras/material-icons/material-icons.css";
import { Quasar, Meta, Loading, Dialog, Notify } from "quasar";

Vue.use(Quasar, {
  // lang: langAr,
  plugins: {
    Meta,
    Loading,
    Dialog,
    Notify,
  },
});
