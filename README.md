# Limitless Wirefeed

The Limitless Wirefeed application in this repo is designed to allow National Societies to download and share pre-prepared content and participant submissions to promote Limitless in both internal and external communication channels (e.g. mailing lists, intranets, social media, staff and volunteer platforms, and meetings).

Major features of Limitless Wirefeed including:

- Download pre-translated communication assets in OneDrive
- Edit and build customized communication assets using Canva or Adobe templates
- View participant submission by each phase
- Filter submissions by language, associated National Society, and tags
- Download all assets of submission for communication purpose including
  - A HD Video with Limitless branding
  - A square video for social media channels with Limitless branding
  - Thumbnails of the submission video
  - Social media sharing posts with all 17 languages supported
  - Video captions in both submission language & English (.vtt format)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
